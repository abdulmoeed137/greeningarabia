@extends('layouts.app')

@section('content')
<style>
    .login{
        display: none !important;
    }
</style>
<div class="login-section">
<div class="container">
    <div class="row foot-row">

        <div class="col-md-6 hidden-xs">
            {{-- <figure class=""
                style="background-image: url({{asset('images/login-bg-2.jpg')}}); height: 500px; background-size: cover; background-repeat: no-repeat;background-position: center;">
    
            </figure> --}}
        </div>
        <div class="col-md-6  " style="">
            <div class="form-sec" style="    padding-bottom: 40px;">
                <div class="section-tittle text-left mb-10 head">
                    <h2 class="form-heading ">{{__('Log In')}}</h2>
             
                {{-- <span style="background: #F5CC44;
                          width: 14%;
                          height: 10px;
                          display: block; color: #F5CC44; margin-bottom: 20px;;">_____</span> --}}
            </div>
            <div class="reg-form">
                <form method="POST" action="{{ route('login') }}">
                    @CSRF
                    <div class="form-group col-md-12">
                        <label for="email">{{__('Email')}}</label>
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror">
                        @error('email')
                       <span class="error-span"> <i class="fa fa-exclamation mr-2 ml-2"></i>{{$message}}</span>
                        @enderror
                    </div>
    
                    <div class="form-group col-md-12">
                        <label for="password">{{__('Password')}}</label>
                        <input name="password" type="password" class="form-control @error('password') is-invalid @enderror">
                        @error('password')
                        <span class="error-span"> <i class="fa fa-exclamation mr-2 ml-2"></i>{{$message}}</span>
                        @enderror
                       
                    </div>
    
              
                  
                    <div class="form-group col-md-12 text-center">
                        {{-- <hr> --}}
                        <div class="header-right-btn f-right d-lg-block ml-30 submit-btn mt-3 mb-5">
                            <button type="submit" class="btn header-btn w-40 border-r">{{__('Log In')}}</button>
                        </div>

                        {{-- <button class="sub-btn">{{__('Log In')}}</button> --}}
                        <br>
                        <br>
                    </div>
                </form>
            </div>
        </div>  
        </div>
        </div>
    </div>
</div>
</div>

@endsection