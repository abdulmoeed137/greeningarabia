<?php

namespace App\Http\Controllers;

use App\Models\MeetingLink;
use App\Models\Registration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Speaker;
use App\Models\Template;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    public function showregsitration()
    {
        return view('auth.register');
    }
    public function registration(Request $request)
    {
        $randomNumber = random_int(100000, 999999);
        $this->validate($request, [
            'name' => 'required|regex:/^([a-zA-Z]+\s)*[a-zA-Z]+$/',
            'country' => 'required|regex:/^([a-zA-Z]+\s)*[a-zA-Z]+$/',
            'email' => 'required|email|unique:users',
            'phonenumber' => 'required|max:15|regex:/^([0-9\+]*)$/',
            'mobilenumber' => 'required|max:15|regex:/^([0-9\+]*)$/',
            'companyname' => 'required',
            'companywebsite' => 'required|regex:/^(?!-)[A-Za-z0-9-]+([\\-\\.]{1}[a-z0-9]+)*\\.[A-Za-z]{2,6}$/',
            'jobtitle' => 'required',
            'reference' => 'required',

            // 'c_password' => 'required|same:password',
        ]);
        //    dd($qr);
        $data = new User();
        $data->name = $request->name;
        $data->country = $request->country;
        $data->email = $request->email;
        $data->password = Hash::make($request->Password);
        $data->phonenumber = $request->phonenumber;
        $data->mobilenumber = $request->mobilenumber;
        $data->companyname = $request->companyname;
        $data->companywebsite = $request->companywebsite;
        $data->jobtitle = $request->jobtitle;
        $data->reference = $request->reference;
        $data->qrcodeid = $randomNumber;
        $data->save();
        $image = QrCode::format('png')
            ->size(200)->errorCorrection('H')
            ->generate($data->qrcodeid);
        $output_file = 'public/qr-code/' .$data->qrcodeid.'.png';
        Storage::disk('local')->put($output_file, $image);
        // $qrcode = QrCode::size(100)->generate($data->qrcodeid);

        // $name = md5($qrcode) . '.png';
        // $path = $qrcode->storeAs("public/qrcode", $name);
        // dd($data->qrcodeid);
        // $image = QrCode::format('png')->merge('../public/assets/img/speakers/Fransisco_Martinez.jpg', 0.3, true)
        //          ->size(200)->errorCorrection('H')
        //          ->generate('A simple example of QR code!');
        //         $output_file = '../public/Qrimg/qr-code/img-' . time() . '.png';
        //         Storage::disk('local')->put($output_file, $image);
        // $randomstring = md5($request->name.time());
        // $qr = QrCode::size(300)->generate('hbskjhvbkj');
        // $qr->storeAs("public/QrCode",$randomstring);

        $template = Template::where('name', 'Registration')->first();
        $details = [

            // 'subject' => $template->subject,
            // 'content'=>$template->content,
            'qrcodeid' => $data->qrcodeid,
            // 'qr'=>$image,
            // 'body' => 'You have successfully registered at King Salman Global Academy.',
            // 'email' => $request->Email,
            // 'name'=>$request->Name,
        ];

        \Mail::to($request->email)->send(new \App\Mail\RegisterMail($details));
        if (\App::getLocale() == "en") {
            toastr()->success('You are Successfully Registered');
            return redirect()->route('register');
        }
        toastr()->success('تم التسجيل بنجاح');
        return redirect()->route('register');
    }

    public function index()
    {
        $speakers = Speaker::all();
        return view('home', compact('speakers'));
    }
    public function dashboard()
    {
        $meetinglink = MeetingLink::count();
        // dd($meetinglink);
        return view('dashboard',compact('meetinglink'));
    }
    public function livelink()
    {
        return view('livelink');
    }
    public function emailtemplate()
    {
        $qrcodeid = User::where('name', 'tahmeer')->get(['qrcodeid']);
        // dd($qrcodeid);
        return view('Admin.emails.register', compact('qrcodeid'));
    }
    public function registration2(){
        return view('auth.register2');
    }
    public function registration_store(Request $request)
    {
        $randomNumber = random_int(100000, 999999);
        $this->validate($request, [
            'name' => 'required|regex:/^([a-zA-Z]+\s)*[a-zA-Z]+$/',
            'country' => 'required|regex:/^([a-zA-Z]+\s)*[a-zA-Z]+$/',
            'email' => 'required|email|unique:users',
            'phonenumber' => 'required|max:15|regex:/^([0-9\+]*)$/',
            'mobilenumber' => 'required|max:15|regex:/^([0-9\+]*)$/',
            'companyname' => 'required',
            'companywebsite' => 'required|regex:/^(?!-)[A-Za-z0-9-]+([\\-\\.]{1}[a-z0-9]+)*\\.[A-Za-z]{2,6}$/',
            'jobtitle' => 'required',
            'reference' => 'required',

            // 'c_password' => 'required|same:password',
        ]);
        //    dd($qr);
        $data = new User();
        $data->name = $request->name;
        $data->country = $request->country;
        $data->email = $request->email;
        $data->password = Hash::make($request->Password);
        $data->phonenumber = $request->phonenumber;
        $data->mobilenumber = $request->mobilenumber;
        $data->companyname = $request->companyname;
        $data->companywebsite = $request->companywebsite;
        $data->jobtitle = $request->jobtitle;
        $data->reference = $request->reference;
        $data->qrcodeid = $randomNumber;
        $data->save();
        // dd($data->qrcodeid);
        $image = QrCode::format('png')
            ->size(200)->errorCorrection('H')
            ->generate($data->qrcodeid);
        $output_file = 'public/qr-code/' .$data->qrcodeid.'.png';
        Storage::disk('local')->put($output_file, $image);
        // $qrcode = QrCode::size(100)->generate($data->qrcodeid);

        // $name = md5($qrcode) . '.png';
        // $path = $qrcode->storeAs("public/qrcode", $name);
        // dd($data->qrcodeid);
        // $image = QrCode::format('png')->merge('../public/assets/img/speakers/Fransisco_Martinez.jpg', 0.3, true)
        //          ->size(200)->errorCorrection('H')
        //          ->generate('A simple example of QR code!');
        //         $output_file = '../public/Qrimg/qr-code/img-' . time() . '.png';
        //         Storage::disk('local')->put($output_file, $image);
        // $randomstring = md5($request->name.time());
        // $qr = QrCode::size(300)->generate('hbskjhvbkj');
        // $qr->storeAs("public/QrCode",$randomstring);

        $template = Template::where('name', 'Registration')->first();
        $details = [

            // 'subject' => $template->subject,
            // 'content'=>$template->content,
            'qrcodeid' => $data->qrcodeid,
            // 'qr'=>$image,
            // 'body' => 'You have successfully registered at King Salman Global Academy.',
            // 'email' => $request->Email,
            // 'name'=>$request->Name,
        ];

        \Mail::to($request->email)->send(new \App\Mail\RegisterMail($details));
        if (\App::getLocale() == "en") {
            toastr()->success('You are Successfully Registered');
            return redirect()->route('register2');
        }
        toastr()->success('تم التسجيل بنجاح');
        return redirect()->route('register2');
    }
}
