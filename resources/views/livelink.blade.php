@extends('layouts.app')

@section('content')
    {{-- <iframe style="display: block;" width="100%" height="450px;" src="https://www.youtube.com/embed?v=ibsJDPbwEL4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> --}}
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            @foreach ($youtubelink as $youtubelink)
                <iframe width="100%" height="409" src="{{ $youtubelink->link }}" title="YouTube video player"
                    frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
            @endforeach
        </div>
    </div>
    
    <br>
    {{-- <div style="width: 50%; margin: auto;">
        <blockquote class="twitter-tweet"><p lang="en" dir="ltr">Sunsets don&#39;t get much better than this one over <a href="https://twitter.com/GrandTetonNPS?ref_src=twsrc%5Etfw">@GrandTetonNPS</a>. <a href="https://twitter.com/hashtag/nature?src=hash&amp;ref_src=twsrc%5Etfw">#nature</a> <a href="https://twitter.com/hashtag/sunset?src=hash&amp;ref_src=twsrc%5Etfw">#sunset</a> <a href="http://t.co/YuKy2rcjyU">pic.twitter.com/YuKy2rcjyU</a></p>&mdash; US Department of the Interior (@Interior) <a href="https://twitter.com/Interior/status/463440424141459456?ref_src=twsrc%5Etfw">May 5, 2014</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
    </div> --}}
    @foreach ($twitterlink as $twitterlink)
        <div style="width: 50%; margin: auto;">
            <blockquote class="twitter-tweet">
                <p lang="en" dir="ltr">Sunsets don&#39;t get much better than this one over.</p>&mdash; US Department of the
                Interior (@Interior) <a href="{{ $twitterlink->link }}">May 5, 2014</a>
            </blockquote>
            <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
        </div>
    @endforeach
@endsection
