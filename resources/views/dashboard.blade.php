@extends('layouts.app')
@section('content')
<section class="banner-img" style="background-image: url({{ asset('assets/img/banner-new.jpg') }});  background-size: cover; background-repeat: no-repeat;background-position: bottom;padding: 40px 0;    padding-bottom: 70px;">
    <section>
        <div class="container pt-0 pb-0">
            <div class="section-content">
                <div style="width: 70%; margin: auto">
                    <br>
                    {{-- <h1 class="title text-white text-uppercase line-bottom mt-0 mb-0 text-center">
                        {{ __('Conference Date') }}
                    </h1> --}}
                    <h2 class="reg-heading">{{__('International Exhibition & Forum On Afforestation Technologies')}}</h2>
                    {{-- <span style="background: #F5CC44;
                                  width: 14%;
                                  margin: auto;
                                  height: 10px;
                                  display: block; color: #F5CC44; text-align: center;">_____</span> --}}
                    <br>
                    <section class="coming-soon">
                        <div>
                            <div class="countdown countdown-2">
                                <div class="container-day child child-first">
                                    <h3  id="days">Time</h3>
                                    <h3 class="time-dis">{{ __('Days') }}</h3>
                                </div>
                                <div class="container-hour child">
                                    <h3 id="hours">Time</h3>
                                    <h3 class="time-dis"> {{ __('Hours') }} </h3>
                                </div>
                                <div class="container-minute child">
                                    <h3  id="minutes">Time</h3>
                                    <h3 class="time-dis">{{ __('Minutes') }}</h3>
                                </div>
                                <div class="container-second child-last">
                                    <h3 id="seconds">Time</h3>
                                    <h3 class="time-dis">{{ __('Seconds') }}</h3>
                                </div>
                            </div>
                            {{-- <div id="aftercountdownbutton">
                                <a href="{{route('livelinks')}}" class="btn btn-info">Join Us</a>
                            </div> --}}
                        </div>
                    </section>

                </div>

                <br>

                

                {{-- <div class="row equal-height-inner mt-sm-0">
                    <div
                        class="col-md-offset-1 col-sm-12 col-md-10 pr-0 pr-sm-15 sm-height-auto mt-sm-0 wow fadeInLeft animation-delay1 mt-120">
                        <div class="sm-height-auto aboutImage"
                            style="background-size: cover;background-position: center;">
                            <div class="p-20" style="position: relative;">



                            </div>
                        </div>
                    </div>

                </div> --}}
            </div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="header-right-btn f-right d-lg-block ml-30 submit-btn mt-5">
                        @if($meetinglink>0)
                        <a href="{{route('livelink')}}" class="btn header-btn w-30 border-r">Join Us</a>
                            @else
                        <a href="{{route('livelink')}}" class="btn header-btn w-30 border-r" style="display: none;">Join Us</a>

                        @endif
                    </div>
                    {{-- <div>
                        <a href="" class="join-button">Join Us</a>
                    </div> --}}
                </div>
            </div>
        </div>
        {{-- <a href="">Submission</a> --}}
    </section>
</section>
@endsection