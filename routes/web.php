<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });
Route::get('/emailtemplate','HomeController@emailtemplate')->name('qrcode');
Route::get('/','HomeController@index')->name('home');
Route::post('lang', function () {
    if (\Session::has('lang') && \Session::get('lang') == 'en') {
        \Session::put('lang', 'ar');
    } else {
        \Session::put('lang', 'en');
    }

    // dd(\App::getLocale());

    return back();
})->name('lang');

Auth::routes();
// Route::get('/registration','HomeController@showregsitration')->name('register');
Route::post('/registration-store', 'HomeController@registration')->name('storeregister');
Route::get('/home','HomeController@index')->name('home');

Route::get('/sign-up','HomeController@registration2')->name('register2');
Route::post('/registration2-store', 'HomeController@registration_store')->name('storeregister2');
Route::middleware('auth')->group(function(){
    Route::get('/dashboard','HomeController@dashboard')->name('dashboard');
    Route::get('/livelinks','DashboardController@livelink')->name('livelink');
});

Route::prefix('admin')->group(function () {
    Route::get('/login', 'Auth\AdminController@ShowLogin')->name('adminlogin');
    Route::post('/login', 'Auth\AdminController@login')->name('checkadminlogin');

    Route::middleware(['auth:admin'])->group(function () {
        Route::get('admin/edit', 'AdminController@edit')->name('admin.edit');
        Route::put('admin/update', 'AdminController@update')->name('admin.update');
        Route::get('', 'AdminController@dashboard')->name('admindashboard');
        Route::post('logout', 'Auth\AdminController@logout')->name('admin.logout');
        Route::get('/registrations','AdminController@Registrations')->name('admin.registrations');
        //  -----------------------Speakers-------------------------------------------------------------
        Route::get('/speakers', 'AdminController@speakers')->name('admin.speakers');
        Route::prefix('speaker')->group(function () {
            Route::get('/create', 'AdminController@speakerForm')->name('admin.speaker.create');
            Route::post('/stored', 'AdminController@speakerCreate')->name('admin.speaker.store');
            Route::post('/approve', 'AdminController@speakerApprove')->name('admin.speaker.approve');
            Route::get('/{id}', 'AdminController@viewSpeaker')->name('admin.speaker.view');
            Route::get('edit/{id}', 'AdminController@editSpeaker')->name('admin.speaker.edit');
            Route::post('update/{id}', 'AdminController@updateSpeaker')->name('admin.speaker.update');
            Route::post('/sessions', 'AdminController@speakerSessions')->name('admin.speaker.sessions');
            Route::post('/session', 'AdminController@speakerSession')->name('admin.speaker.session');
            Route::post('/meeting', 'AdminController@speakerMeeting')->name('admin.speaker.meeting');
    
        });
        Route::delete('speakers/{speaker}', 'AdminController@deleteSpeakers')->name('admin.speaker.delete');
        Route::post('speakers/{speaker}/restore', 'AdminController@restoreSpeakers')->name('admin.speaker.restore');
        //  -----------------------Links-------------------------------------------------------------
    Route::get('links', 'AdminController@links')->name('admin.links');
    Route::prefix('link')->group(function () {
        Route::post('/create', 'AdminController@linkCreate')->name('admin.link.create');
        Route::post('/update/{id}', 'AdminController@linkUpdate')->name('admin.link.update');
        Route::get('/toggle/status', 'AdminController@toggleLink')->name('admin.link.toggle');
    });
        
    });
});
