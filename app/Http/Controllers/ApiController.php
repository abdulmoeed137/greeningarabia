<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ApiController extends Controller
{
    public function userdetail($qrcodeid){
        $response=[];
        $msg=[];
        // return response()->json(['error'=>"user Found"], 200);
        $userdetail = User::where('qrcodeid', $qrcodeid)->first();
        
        if(!isset($userdetail)){

        $msg[]='User Not Found';
        return response()->json([
            'msg' => $msg,
        ],404); 
        }
           $msg[]='User Found';
        $response[]=$userdetail;
           return response()->json([
            'result' => $response,
            'msg'=> $msg,
        ],200);
        
            
    }
}
