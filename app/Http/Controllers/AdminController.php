<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Registration;
use App\Models\User;
use App\Models\MeetingLink;
use App\Models\Setting;
use App\Models\Speaker;
use Hash;


class AdminController extends Controller
{
    public function edit()
    {
        $user = auth()->user();
        return view('Admin.edit', compact('user'));
    }

    public function update(Request $request)
    {
        $user = auth()->user();
        $rules = [
            'name' => 'required|string|min:2|max:255',
            'email' => 'required|email:filter|max:255|unique:users,email'
        ];
        $msg = 'Profile info updated successfully';
        if ($request->hasAny(['old_password', 'password', 'confirmation_password'])) {
            $rules = [
                'old_password' => 'required|min:8|max:255',
                'password' => 'required|min:8|max:255',
            ];
            $msg = 'Profile password changed successfully';
        }
        $validated = $request->validate($rules);
        if ($request->hasAny(['old_password', 'password', 'confirmation_password'])) {
            $user->update(['password' => Hash::make($validated['password'])]);
        } else {
            $user->update([
                'name' => $validated['name'],
                'email' => $validated['email']
            ]);
        }
        toastr()->success($msg);
        return back();
    }
    public function dashboard()
    {
        // ---------------------------------------Graph-----------------------------------------
        $per_day_registrations = DB::table('users')->select('created_at', DB::raw("COUNT(id) as users_count"))->groupBy(DB::raw("DATE(created_at)"))->get();
// dd("this is here");
        $label = [];
        $graph_data = [];
        $colours = [];
        
        foreach ($per_day_registrations as $per_day_attendant) {
            $label[] = date('d', strtotime($per_day_attendant->created_at)) . '-' . date('M', strtotime($per_day_attendant->created_at));
            $graph_data[] = $per_day_attendant->users_count;
            // $name[] = $per_day_attendant->name;
            $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
        }

        $chart = new Registration;
        $chart->labels = ($label);
        $chart->dataset = ($graph_data);
        $chart->colours = $colours;
        // $chart->name = ($name);
        // ---------------------------------------Graph-----------------------------------------

               // ---------------------------------------Graph2-----------------------------------------
               $reference_registrations = DB::table('users')->select('reference', DB::raw("COUNT(id) as users_count"))->groupBy(DB::raw("reference"))->get();
               // dd("this is here");
                       $label = [];
                       $graph_data = [];
                       $colours = [];
                       
                       foreach ($reference_registrations as $per_reference_attendant) {
                           $label[] = $per_reference_attendant->reference;
                           $graph_data[] = $per_reference_attendant->users_count;
                           // $name[] = $per_day_attendant->name;
                           $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
                       }
               
                       $chart2 = new User;
                       $chart2->labels = ($label);
                       $chart2->dataset = ($graph_data);
                       $chart2->colours = $colours;
                       // $chart->name = ($name);
                       // ---------------------------------------Graph2-----------------------------------------
               

        $registrations = User::count();
        // $submissions = Submission::count();
        // $sessions = Session::count();
        // $writers = Writer::count();
        // $users = User::count();
        return view('Admin.dashboard',compact('chart','chart2','registrations'));
    }
    public function Registrations()
    {
        $registrations = User::all();

        // dd(Submission::with('user')->get());
        // $ids = $submission->pluck('id')->toArray();
        // $templates = Template::where('status', 1)->orderBy('name')->latest()->get();
        // if ($request->ajax()) {
        //     return \DataTables::of($submission)
        //         ->addColumn('selection', function ($row) {
        //             return "<input type='checkbox' class='sender' name='user" . $row->id . "' id='user" . $row->id . "' value='" . $row->id . "' >";
        //         })

        //         ->addColumn('created_at', function ($row) {
        //             return $row->created_at->format('d-M-Y H:i a');
        //         })
        //         ->rawColumns(['selection'])
        //         ->make(true);
        // }
        return view('Admin.Registration.registration', compact('registrations'));
    }
     // ----------------------------------------Speakers---------------------------------------

     public function speakers()
     {
         $speakers = Speaker::withTrashed()->latest()->get();
         // dd($speakers);
         return view('Admin.Speaker.index', compact('speakers'));
     }
 
     public function speakerForm()
     {
         return view('Admin.Speaker.create');
     }
 
     public function speakerCreate(Request $request)
     {
         $request->validate([
             'image' => 'required|file|mimes:png,jpg',
             'ar_name' => 'required|string|max:255',
             'ar_title' => 'required|string|max:255',
             'ar_description' => 'string',
             'en_name' => 'required|string|max:255',
             'en_title' => 'required|string|max:255',
             'en_description' => 'string',
         ]);
         $speaker = new Speaker();
         $name = md5($request->en_name . time()) . ".{$request->image->getClientOriginalExtension()}";
         $path = $request->image->storeAs("public/speakers", $name);
         $speaker->image = str_replace('public', 'storage', $path);
         $speaker->ar = [
             'name' => $request->ar_name,
             'title' => $request->ar_title,
             'description' => $request->ar_description,
         ];
         $speaker->en = [
             'name' => $request->en_name,
             'title' => $request->en_title,
             'description' => $request->en_description,
         ];
 
         $speaker->save();
         toastr()->success("Speaker created successfully");
 
         // $this->sendPasswordEmail($data['email'], $password);
         return redirect()->to(route('admin.speakers'));
     }
 
     public function editSpeaker($id)
     {
         $speaker = Speaker::findOrFail($id);
         return view('Admin.Speaker.update', compact('speaker'));
     }
 
     public function updateSpeaker(Request $request, $id)
     {
         $speaker = Speaker::findOrFail($id);
         $request->validate([
             'image' => 'nullable|file|mimes:png,jpg',
             'ar_name' => 'required|string|max:255',
             'ar_title' => 'required|string|max:255',
             'ar_description' => 'string',
             'en_name' => 'required|string|max:255',
             'en_title' => 'required|string|max:255',
             'en_description' => 'string',
         ]);
 
         if ($request->has('image')) {
             $url = public_path("/storage/speakers/");
             $parts = explode('/', $speaker->image);
             $url .= end($parts);
             @unlink($url);
 
             $name = md5($request->en_name . time()) . ".{$request->image->getClientOriginalExtension()}";
             $path = $request->image->storeAs("public/speakers", $name);
             $speaker->image = str_replace('public', 'storage', $path);
         }
         $speaker->ar = [
             'name' => $request->ar_name,
             'title' => $request->ar_title,
             'description' => $request->ar_description,
         ];
         $speaker->en = [
             'name' => $request->en_name,
             'title' => $request->en_title,
             'description' => $request->en_description,
         ];
 
         $speaker->save();
         toastr()->success("Speaker has successfully updated");
         return redirect()->route('admin.speakers')->with('message','Speaker has successfully updated');
     }
 
     public function deleteSpeakers(Speaker $speaker)
     {
         $speaker->delete();
         toastr()->success("Speaker deleted successfully");
         return back()->with('message','Speaker deleted successfully');
     }
     public function restoreSpeakers($id)
     {
         $speaker = Speaker::withTrashed()->findOrFail($id);
         $speaker->restore();
         toastr()->success("Speaker restored successfully");
         return back()->with('message','Speaker restored successfully');
     }
    // -----------------------Links Admin Panel-------------------------
    public function links()
    {
        $links = MeetingLink::all();
        $active = Setting::firstOrCreate(
            ['title' => 'active_links'],
            ['value' => 0]
        )->value;
        return view('Admin.links', compact('links', 'active'));
    }

    public function toggleLink()
    {
        $active = Setting::where('title', 'active_links')->first();
        $active->update(['value' => $active->value == 1 ? 0 : 1]);
        toastr()->success('Links active status changed successfully');
        return back();
    }

    public function linkCreate(Request $request)
    {
        MeetingLink::create($request->all());
        toastr()->success('Meeting Link has successfuly added');
        return back();
    }

    public function linkUpdate(Request $request, $id)
    {
        $data = $request->all();
        MeetingLink::find($id)->update($data);
        toastr()->success('Meeting Link has successfuly updated');
        return back();
    }
}
